<%@page import="java.util.Date"%>
<jsp:useBean class="pratica.jsp.loginBean" id="info" scope="session"/>
<jsp:setProperty name="info" property="*" />

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Login</title> 
    </head> 
    <body>
        <form method="post" action="/pratica-jsp/login.jsp">
            Código: <input type="text" name="login"/><br/>
            Nome: <input type="password" name="senha"/><br/>
            Perfil: <select name="perfil">
                <option value="1">Cliente</option> 
                <option value="2">Gerente</option>
                <option value="3">Administrador</option>
            </select>
            
            <input type="submit" value="Enviar"/>
        </form>
    </body>
</html>

<% 
    if (request.getMethod().equals("POST")) {
        if(!info.getLogin().isEmpty() && info.getLogin().equals(info.getSenha())) {
            String perfilName = "";
            switch(info.getPerfil()) {
                case 1:
                    perfilName = "Cliente";
                    break;
                case 2:
                    perfilName = "Gerente";
                    break;
                case 3:
                    perfilName = "Administrador";
                    break;
            }
            
            java.util.Date data = new java.util.Date();
            
            out.println("<div><font size=\"3\" color=\"blue\">"
                    + perfilName
                    + ", login bem sucedido, para "
                    + info.getLogin()
                    + " "
                    + data.getHours()
                    +":"
                    + data.getMinutes()
                    +":"
                    + data.getSeconds()
                    + "</font></div>");
        } else {
            out.println("<div><font size=\"3\" color=\"red\">Acesso negado</font></div>");
        }
    } 
%>